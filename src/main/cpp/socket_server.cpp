#include "src/main/cpp/socket_server.h"

#include <folly/io/async/AsyncSocketException.h>

#include <iostream>
#include <thread>

namespace dslib {

namespace socket {

std::atomic_size_t SocketServer::AcceptCallback::ConnectionHandler::current_id =
    0;

/**
 * Connection accepted
 * --> connectionAccepted
 * --> create connection handler
 *     + create read_callback
 *     + create write_callback
 *     + set read_callback for socket
 * --> read data available in socket
 * --> success --> call read success callback
 * --> call HandleRequest
 * --> write output of HandleRequest to socket
 * --> success --> call write success callback
 * --> remove socket and connection handler
 *
 * All the callbacks are run on the same thread, so synchronize them would be
 * unnecessary.
 */
void SocketServer::AcceptCallback::connectionAccepted(
    folly::NetworkSocket fd, const folly::SocketAddress &client_addr) noexcept {
  auto socket = folly::AsyncSocket::newSocket(event_base_, fd);
  auto read_callback = std::make_shared<ReadCallback>();
  auto write_callback = std::make_shared<WriteCallback>();
  socket->setReadCB(read_callback.get());

  conn_handlers_.emplace_back(read_callback, write_callback, std::move(socket));

  LOG(INFO) << "Received connection from " << client_addr.getHostStr() << ":"
            << client_addr.getPort()
            << " socket_count: " << conn_handlers_.size()
            << " connection_id: " << conn_handlers_.back().id
            << " handling thread: " << std::this_thread::get_id();
  auto it = std::prev(conn_handlers_.end());

  write_callback->setSuccessCallback([this, it]() {
    it->pending_write = false;
    if (it->eof_reached) {
      RemoveConnection(it);
    }
  });
  write_callback->setErrorCallback(
      [this, it](size_t, const folly::AsyncSocketException &ex) {
        it->pending_write = false;
        server_->HandleError(ex);
        RemoveConnection(it);
      });

  read_callback->setReadCallback([this, it, client_addr](const char *buffer,
                                                         size_t len) mutable {
    it->pending_write = true;
    server_->HandleRequest(event_base_, client_addr, buffer, len)
        .thenValue([it, ev = event_base_](const std::string &response) mutable {
          // Write response
          ev->runInEventBaseThread([it, response]() mutable {
            (*it).async_socket->write((*it).write_callback.get(),
                                      response.c_str(), response.size());
          });
        });
  });

  read_callback->setEOFCallback([this, it]() {
    it->eof_reached = true;
    if (!it->pending_write) RemoveConnection(it);
  });
  read_callback->setErrorCallback(
      [this, it](const folly::AsyncSocketException &ex) {
        server_->HandleError(ex);
        RemoveConnection(it);
      });
}

void SocketServer::InitializeEventBase(
    folly::EventBase *main_evb, const std::vector<folly::EventBase *> evbs) {
  CHECK(!evbs.empty()) << "Need at least 1 thread for handling connection";
  // Main thread for listening
  main_event_base_ = main_evb;
  attachEventBase(main_event_base_);

  // Other threads for handling connections
  for (auto &event_base : evbs) {
    // One callback object for one thread/event loop, which will be called in
    // round-robin fashion
    auto ac = std::make_shared<AcceptCallback>(*this, event_base);
    callbacks_.emplace_back(
        std::dynamic_pointer_cast<folly::AsyncServerSocket::AcceptCallback>(
            ac));
    addAcceptCallback(callbacks_.back().get(), event_base);
  }
}

void SocketServer::Destroy() { destroy(); }

void SocketServer::Start(int port, int backlog) {
  bind(port);
  listen(backlog);
  startAccepting();
  LOG(INFO) << "Server listening on port " << port;
}

}  // namespace socket

}  // namespace dslib
