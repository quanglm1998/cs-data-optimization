#pragma once

#include <folly/SocketAddress.h>
#include <folly/String.h>
#include <folly/io/async/AsyncSocket.h>
#include <folly/io/async/AsyncSocketException.h>

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <filesystem>
#include <fstream>
#include <memory>
#include <thread>

#include "protobuf/cs_data.pb.h"
#include "src/main/cpp/socket_server.h"

namespace cs_data {

class CsDataServer : public dslib::socket::SocketServer {
 protected:
  folly::Future<std::string> HandleRequest(const folly::SocketAddress &,
                                           const char *buffer,
                                           size_t len) noexcept override {
    std::string buffer_string(buffer);

    cs_data::CsQuery query;
    if (!query.ParseFromString(buffer_string)) {
      LOG(INFO) << "Cannot parse query!";
    }

    auto start_date = query.start_date();
    auto end_date = query.end_date();

    auto start = ConvertStringToPtime(start_date);
    auto end = ConvertStringToPtime(end_date);

    cs_data::CsReplyData reply_data;

    // loops through all date from `start_date` to `end_date` gets all the
    // infomation by reading the corresponding record.* and sec_master.* files.
    for (auto date = start; date <= end; date = GetNextDate(date)) {
      auto current_date = ConvertPtimeToString(date);
      auto year = GetYear(current_date);
      auto month = GetMonth(current_date);
      auto record_date =
          FormatDate("data/cs_data", year, month, "record", current_date);
      auto sec_master_date =
          FormatDate("data/cs_data", year, month, "sec_master", current_date);

      cs_data::CsDailyData daily_data;
      daily_data.set_date(std::stoi(current_date));

      AddRecord(record_date, daily_data);
      AddSecMaster(sec_master_date, daily_data);

      if (!daily_data.records().empty() || !daily_data.sec_master().empty()) {
        auto last_daily_data = reply_data.add_daily_data();
        *last_daily_data = daily_data;
      }
    }

    return reply_data.SerializeAsString();
  }

  void HandleError(const std::exception &ex) noexcept override {
    LOG(INFO) << "Connection error " << ex.what();
  }

 private:
  boost::posix_time::ptime GetNextDate(const boost::posix_time::ptime &date) {
    boost::posix_time::time_iterator it(date, boost::posix_time::hours(24));
    ++it;
    return *it;
  }

  std::string ConvertPtimeToString(boost::posix_time::ptime &t) {
    return boost::posix_time::to_iso_string(t).substr(0, 8);
  }

  boost::posix_time::ptime ConvertStringToPtime(const std::string &date) {
    return boost::posix_time::ptime(
        boost::gregorian::from_undelimited_string(date),
        boost::posix_time::hours(0));
  }

  std::string FormatDate(const std::string &path, const std::string &year,
                         const std::string &month, const std::string &type,
                         const std::string &current_date) {
    return path + "/" + year + "/" + month + "/" + type + "." + current_date;
  }

  std::string GetYear(const std::string &current_date) {
    return current_date.substr(0, 4);
  }

  // non-zero starting month
  std::string GetMonth(const std::string &current_date) {
    auto month = current_date.substr(4, 2);
    if (month[0] == '0') month = month.substr(1);
    return month;
  }

  // adds to `daily_data` all record information in `file_path`.
  void AddRecord(const std::string &file_path,
                 cs_data::CsDailyData &daily_data) {
    if (!std::filesystem::exists(file_path)) return;
    std::ifstream ifs(file_path);
    std::string current_line;
    while (getline(ifs, current_line)) {
      std::vector<std::string> tokens;
      folly::split(",", current_line, tokens);
      if (tokens.size() != 3 || tokens[0].empty() || tokens[1].empty()) {
        LOG(INFO) << "INCORRECT FORMAT: record need to have 3 tokens, "
                     "seperated by commas!";
        LOG(INFO) << current_line;
        continue;
      }

      cs_data::CsRecord record;

      record.set_gvkey(tokens[0]);

      // sometimes `var_name` can be enclosed in double quotation mark
      if (tokens[1][0] == '\"') {
        if (tokens[1].back() != '\"') {
          LOG(INFO) << "INCORRECT FORMAT: expected \" at the end of var_name!";
          LOG(INFO) << tokens[1];
          continue;
        }
        tokens[1] = tokens[1].substr(1, tokens[1].size() - 2);
        if (tokens[1].empty()) {
          LOG(INFO) << "INCORRECT FORMAT: var_name can't be empty!";
          LOG(INFO) << tokens[1];
          continue;
        }
      }

      record.set_var_name(tokens[1]);
      record.set_data(tokens[2]);

      auto last_record = daily_data.add_records();
      *last_record = record;
    }
    ifs.close();
  }

  // adds to `daily_data` all sec_master information in `file_path`.
  void AddSecMaster(const std::string &file_path,
                    cs_data::CsDailyData &daily_data) {
    if (!std::filesystem::exists(file_path)) return;
    std::ifstream ifs(file_path);
    std::string current_line;
    while (getline(ifs, current_line)) {
      std::vector<std::string> tokens;
      folly::split(",", current_line, tokens);
      if (tokens.size() != 4 || tokens[0].empty() || tokens[1].empty()) {
        LOG(INFO) << "INCORRECT FORMAT: sec_master need to have 4 tokens, "
                     "seperated by commas!";
        LOG(INFO) << current_line;
        continue;
      }
      cs_data::CsSecMaster sec_master;

      sec_master.set_gvkey(tokens[0]);
      sec_master.set_id(tokens[1]);
      sec_master.set_symbol(tokens[2]);
      sec_master.set_ticker(tokens[3]);

      auto last_sec_master = daily_data.add_sec_master();
      *last_sec_master = sec_master;
    }
    ifs.close();
  }

  std::vector<folly::AsyncSocket::UniquePtr> sockets_;
};

}  // namespace cs_data
