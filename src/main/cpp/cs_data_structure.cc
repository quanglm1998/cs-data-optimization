#include "src/main/cpp/cs_data_structure.h"

namespace cs_data {

void CsDataStructure::Update(const cs_data::CsDailyData &daily_data) {
  DataType temp;
  std::set<std::string> gvkeys;
  // update all gvkey's temp map
  for (const auto &record : daily_data.records()) {
    const auto &gvkey = record.gvkey();
    gvkeys.insert(gvkey);

    const auto &var_name = record.var_name();

    std::vector<std::string> data_points;
    folly::split(";", record.data(), data_points);

    for (const auto &data_point : data_points) {
      std::vector<std::string> tokens;
      folly::split(":", data_point, tokens);
      if (tokens.size() != 2) {
        LOG(INFO) << "data format is not correct \"" << data_point << '\"';
        continue;
      }

      // add pair date-value to maps
      if (int_var_names_.find(var_name) != int_var_names_.end()) {
        AddValueToMap(temp.int_map, gvkey, var_name, tokens[0],
                      std::stoi(tokens[1]));
      } else if (string_var_names_.find(var_name) != string_var_names_.end()) {
        AddValueToMap(temp.string_map, gvkey, var_name, tokens[0], tokens[1]);
      } else if (IsDoubleVarName(var_name)) {
        double_var_names_.insert(var_name);
        AddValueToMap(temp.double_map, gvkey, var_name, tokens[0],
                      std::stod(tokens[1]));
      } else {
        LOG(INFO) << "var_name " << var_name << " has not been supported!";
        continue;
      }
    }
  }

  // update gvkey map from temp
  for (const auto &gvkey : gvkeys) {
    UpdateMaps(temp, gvkey_map_, gvkey, gvkey);
  }

  // update sec_master
  // the first appearance ids will inherit all gvkey info
  std::set<std::string> first_appearance;
  for (const auto &sec_master : daily_data.sec_master()) {
    const auto &id = sec_master.id();
    const auto &gvkey = sec_master.gvkey();
    if (id_to_gvkey_.find(id) == id_to_gvkey_.end()) {
      UpdateMaps(gvkey_map_, id_map_, gvkey, id);
      first_appearance.insert(id);
    }
    id_to_gvkey_[id] = gvkey;
  }
  for (const auto &[id, gvkey] : id_to_gvkey_) {
    if (first_appearance.find(id) != first_appearance.end())
      UpdateMaps(temp, id_map_, gvkey, id);
  }
}

bool CsDataStructure::IsDoubleVarName(const std::string &var_name) {
  if (var_name.size() >= 1 &&
      (var_name.back() == 'Q' || var_name.back() == 'Y'))
    return true;
  if (var_name.size() >= 2 && var_name.substr(var_name.size() - 2) == "SA")
    return true;
  return false;
}

void CsDataStructure::UpdateMaps(DataType &src, DataType &sink,
                                 const std::string &src_id,
                                 const std::string &sink_id) {
  // double
  UpdateMap(src_id, sink_id, double_var_names_, src.double_map,
            sink.double_map);
  // int
  UpdateMap(src_id, sink_id, int_var_names_, src.int_map, sink.int_map);
  // string
  UpdateMap(src_id, sink_id, string_var_names_, src.string_map,
            sink.string_map);
}

CsDataStructure::DataType CsDataStructure::Get() {
  DataType res = id_map_;

  // induce IBSA and IBQ from IBY
  for (const auto &[id, gvkey] : id_to_gvkey_) {
    if (res.double_map.find(std::pair(id, "IBY")) == res.double_map.end())
      continue;

    for (const auto &[date, val] : res.double_map[std::pair(id, "IBY")]) {
      auto year = date.substr(0, 4);
      auto month_date = date.substr(4);
      auto it = std::find(end_quarter_dates_.begin(), end_quarter_dates_.end(),
                          month_date);

      if (it == end_quarter_dates_.end()) {
        LOG(INFO) << month_date << " is not in quarters";
      }

      if (it == end_quarter_dates_.end() || it == end_quarter_dates_.begin())
        continue;

      auto InMap = [&](const std::string &var_name, const std::string &date) {
        if (res.double_map.find(std::pair(id, var_name)) ==
            res.double_map.end())
          return false;
        if (res.double_map[std::pair(id, var_name)].find(date) ==
            res.double_map[std::pair(id, var_name)].end()) {
          return false;
        }
        return true;
      };

      // IBQ
      {
        auto last_date = year + *(it - 1);
        if (!InMap("IBQ", date) && InMap("IBY", last_date)) {
          auto new_val = val - res.double_map[std::pair(id, "IBY")][last_date];
          res.double_map[std::pair(id, "IBQ")][date] = new_val;
        }
      }

      // IBSA
      if (it - end_quarter_dates_.begin() == 3) {
        auto last_date = year + *(it - 2);
        if (!InMap("IBSA", date) && InMap("IBY", last_date)) {
          auto new_val = val - res.double_map[std::pair(id, "IBY")][last_date];
          res.double_map[std::pair(id, "IBSA")][date] = new_val;
        }
      }
    }
  }
  return res;
}

}  // namespace cs_data