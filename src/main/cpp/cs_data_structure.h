#pragma once

#include <folly/String.h>
#include <glog/logging.h>

#include <map>
#include <set>
#include <string>
#include <vector>

#include "protobuf/cs_data.pb.h"

namespace cs_data {

class CsDataStructure {
 public:
  CsDataStructure(const std::set<std::string> &int_var_names,
                  const std::set<std::string> &string_var_names,
                  const std::vector<std::string> &end_quarter_dates)
      : int_var_names_(int_var_names),
        string_var_names_(string_var_names),
        end_quarter_dates_(end_quarter_dates) {}

  using KeyType = std::pair<std::string, std::string>;
  template <typename T>
  using MapType = std::map<KeyType, std::map<std::string, T>>;

  // map pair of (id, var_name) to data, consisting of day and value
  struct DataType {
    MapType<std::string> string_map;
    MapType<int> int_map;
    MapType<double> double_map;
  };

  void Update(const cs_data::CsDailyData &daily_data);

  DataType Get();

 private:
  template <typename T>
  void AddValueToMap(MapType<T> &value_map, const std::string &gvkey,
                     const std::string &var_name, const std::string &date,
                     const T &value) {
    value_map[std::pair(gvkey, var_name)][date] = value;
  }

  // add all values of (src_id, var_name) pair from src_map to sink_map
  template <typename T>
  void UpdateMap(const std::string &src_id, const std::string &sink_id,
                 std::set<std::string> &var_names, MapType<T> &src_map,
                 MapType<T> &sink_map) {
    for (const auto &var_name : var_names) {
      if (auto it = src_map.find(std::pair(src_id, var_name));
          it != src_map.end()) {
        for (const auto &[date, value] : it->second) {
          sink_map[std::pair(sink_id, var_name)][date] = value;
        }
      }
    }
  }

  void UpdateMaps(DataType &src, DataType &sink, const std::string &src_id,
                  const std::string &sink_id);

  bool IsDoubleVarName(const std::string &var_name);

  DataType gvkey_map_;
  DataType id_map_;

  // instruction's id to gvkey
  std::map<std::string, std::string> id_to_gvkey_;

  // set during Update
  std::set<std::string> double_var_names_;
  // set at constructor
  std::set<std::string> int_var_names_;
  std::set<std::string> string_var_names_;

  // make sure it's sorted and has the size of 4
  std::vector<std::string> end_quarter_dates_;
};

}  // namespace cs_data