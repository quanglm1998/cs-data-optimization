#include <folly/executors/IOThreadPoolExecutor.h>
#include <folly/executors/ThreadedExecutor.h>
#include <folly/futures/Future.h>

#include <iostream>
#include <libconfig.h++>

#include "src/main/cpp/cs_data_client.h"

static bool ValidateDate(const char *flagname, const std::string &str) {
  if (str.empty()) LOG(INFO) << flagname << " can't be empty!";
  return !str.empty();
}

DEFINE_string(host, "127.0.0.1", "host to send request to");
DEFINE_int32(port, 3333, "Port");
DEFINE_string(start_date, "", "start date");
DEFINE_string(end_date, "", "end date");
DEFINE_validator(start_date, &ValidateDate);
DEFINE_validator(end_date, &ValidateDate);
DEFINE_string(config, "", "Path to config file");
DEFINE_validator(config, &ValidateDate);

void LoadConfig(const std::string &config_path,
                std::set<std::string> &int_var_names,
                std::set<std::string> &string_var_names,
                std::vector<std::string> &end_quarter_dates) {
  libconfig::Config cfg;

  // Read the file. If there is an error, report it and exit.
  try {
    cfg.readFile(config_path);
  } catch (const libconfig::FileIOException &fioex) {
    LOG(INFO) << "I/O error while reading file";
  } catch (const libconfig::ParseException &pex) {
    LOG(INFO) << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError();
  }

  const libconfig::Setting &root = cfg.getRoot();
  try {
    for (auto &value : root["int_var_names"]) int_var_names.insert(value);
    for (auto &value : root["string_var_names"]) string_var_names.insert(value);
    for (auto &value : root["end_quarter_dates"])
      end_quarter_dates.push_back(value);
  } catch (const libconfig::SettingNotFoundException &nfex) {
    LOG(FATAL) << "Can't load config";
  }
}

int main(int argc, char *argv[]) {
  FLAGS_logtostderr = true;
  // Initialize Google's logging library.
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  folly::IOThreadPoolExecutor executor(1);

  std::set<std::string> int_var_names;
  std::set<std::string> string_var_names;
  std::vector<std::string> end_quarter_dates;
  LoadConfig(FLAGS_config, int_var_names, string_var_names, end_quarter_dates);

  auto event_base = executor.getEventBase();
  auto client = std::make_shared<cs_data::CsDataClient>(event_base);
  event_base->runInEventBaseThread(folly::Func([&]() mutable {
    client->Init(FLAGS_start_date, FLAGS_end_date, int_var_names,
                 string_var_names, end_quarter_dates);
    client->Connect(FLAGS_host, FLAGS_port);
  }));
  // Put main thread to sleep forever
  folly::Promise<int>().getFuture().wait();
  return 0;
}
