#pragma once

#include <folly/SocketAddress.h>
#include <folly/String.h>
#include <folly/io/async/AsyncSocket.h>
#include <folly/io/async/AsyncSocketException.h>

#include <iostream>
#include <memory>
#include <thread>

#include "protobuf/cs_data.pb.h"
#include "src/main/cpp/cs_data_structure.h"
#include "src/main/cpp/socket_client.h"

namespace cs_data {

class CsDataClient : public dslib::socket::SocketClient {
 public:
  explicit CsDataClient(folly::EventBase *evb) : SocketClient(evb) {}

  folly::Future<std::string> GetWriteContent() noexcept override {
    cs_data::CsQuery query;
    query.set_start_date(start_date_);
    query.set_end_date(end_date_);
    return query.SerializeAsString();
  }

  void HandleResponse(const char *buf, size_t len) noexcept override {
    std::string string_buf(buf);
    cs_data::CsReplyData reply_data;
    reply_data.ParseFromString(string_buf);
    CsDataStructure ds(int_var_names_, string_var_names_, end_quarter_dates_);
    for (auto daily_data : reply_data.daily_data()) {
      ds.Update(daily_data);
#ifdef DEBUG
      Debug(ds.Get());
#endif
    }
  }

  void Init(const std::string &start_date, const std::string &end_date,
            const std::set<std::string> &int_var_names,
            const std::set<std::string> &string_var_names,
            const std::vector<std::string> &end_quarter_dates) {
    start_date_ = start_date;
    end_date_ = end_date;
    int_var_names_ = int_var_names;
    string_var_names_ = string_var_names;
    end_quarter_dates_ = end_quarter_dates;
  }

 private:
  void Debug(const CsDataStructure::DataType &ds) {
    for (const auto &[key, value] : ds.double_map) {
      std::string now = "{" + key.first + ", " + key.second + "}: [";
      for (const auto &[date, v] : value) {
        now += "(" + date + ", " + std::to_string(v) + "), ";
      }
      now += "]";
      LOG(INFO) << now;
    }

    for (const auto &[key, value] : ds.int_map) {
      std::string now = "{" + key.first + ", " + key.second + "}: [";
      for (const auto &[date, v] : value) {
        now += "(" + date + ", " + std::to_string(v) + "), ";
      }
      now += "]";
      LOG(INFO) << now;
    }

    for (const auto &[key, value] : ds.string_map) {
      std::string now = "{" + key.first + ", " + key.second + "}: [";
      for (const auto &[date, v] : value) {
        now += "(" + date + ", " + v + "), ";
      }
      now += "]";
      LOG(INFO) << now;
    }
  }

  std::string start_date_;
  std::string end_date_;

  std::set<std::string> int_var_names_;
  std::set<std::string> string_var_names_;

  // make sure it's sorted and has the size of 4
  std::vector<std::string> end_quarter_dates_;
};

}  // namespace cs_data
