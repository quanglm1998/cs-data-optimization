#pragma once

#include <folly/SocketAddress.h>
#include <folly/io/async/AsyncSocket.h>
#include <folly/io/async/AsyncSocketException.h>

#include <memory>
#include <thread>

#include "src/main/cpp/socket_server.h"

namespace dslib {

namespace socket {

class EchoServer : public SocketServer {
 protected:
  folly::Future<std::string> HandleRequest(const folly::SocketAddress &,
                                           const char *buffer,
                                           size_t len) noexcept override {
    std::string content = std::to_string(len);
    return "HTTP/1.1 200 OK\nContent-Length: " +
           std::to_string(content.size() + 2) + "\n\nOK" + content;
  }

  void HandleError(const std::exception &ex) noexcept override {
    LOG(INFO) << "Connection error " << ex.what();
  }

 private:
  std::vector<folly::AsyncSocket::UniquePtr> sockets_;
};

}  // namespace socket

}  // namespace dslib
