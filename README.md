# How to run

- Server:
```console
bazel run src/main/cpp:main
```
- Client:
```console
bazel run src/main/cpp:client -- -start_date={start_date} -end_date={end_date}
```

To debug out data:

```console
bazel run --copt="-DDEBUG" src/main/cpp:client -- -start_date={start_date} -end_date={end_date}
```

After received the reply message, client will update and print out data